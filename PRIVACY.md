# Datenschutzerklärung

Diese Datenschutzerklärung gillt für die Nutzung unseres Online-Angebots "TestApp" unter https://testapp.ga/ und die dazugehörigen Mobil-Apps (nachfolgend "Anwendung" oder "Website").

Jegliche Verarbeitung Ihrer personenbezogenen Daten (nachfolgend auch "Daten") geschieht unter Beachtung der geltenden datenschutzrechtlichen Vorschriften, insbesondere der Datenschutzgrundverordnung (DSGVO).

## 1) Verantwortlicher

Verantwortlicher für die Erhebung, Verarbeitung und Nutzung Ihrer personenbezogenen Daten im Sinne von Art. 4 Nr. 7 DSGVO ist

    Jasper Michalke
    August-Kirch-Straße 15j
    22525 Hamburg

    Email: info@testapp.ga

Wenn Sie der Erhebung, Verarbeitung oder Nutzung Ihrer Daten durch insgesamt oder für einzelne Maßnahmen widersprechen wollen, können Sie Ihren Widerspruch an den Verantwortlichen richten.

Sie können diese Datenschutzerklärung jederzeit speichern und ausdrucken.

## 2) Allgemeine Zwecke der Verarbeitung

Wir verarbeiten ihre Daten, damit Sie unsere Anwendung verwenden können sowie zur Abwehr unerwünschter Anfragen an unsere Server.

## 3) Welche Daten wir verwenden und warum

### 3.1) Protokolldaten

Durch die Verwendung unserer Anwendung werden folgende Daten erhoben und gespeichert:

 * Browsertyp und Browserversion
 * Betriebssystem
 * IP-Adresse

Ihre IP-Adresse wird annonymisiert. Das bedeutet, dass ein Teil von dieser abgeschnitten wird, damit diese nicht eindeutig zuordbar ist.

Wir nutzen diese Protokolldaten ohne Zuordnung zu Ihrer Person oder sonstiger Profilerstellung sondern nur Zwecks der Abwehr ungewollter Anfragen an unsere Server.

Hierin liegt auch unser berechtigtes Interesse gemäß Art 6 Abs. 1 S. 1 f) DSGVO.

### 3.2) Cookies

Wenn Sie unsere Anwendung verwenden, werden keine Cookies gesetzt. Um die Sicherheit und den Schutz unserer Nutzer zu gewährleisten, haben wir uns entschieden, von jeglicher Verwendung von Cookies abzusehen.

### 3.3) Nutzerkonto

Sie können in unsere Anwendung ein Nutzerkonto anlegen. Wünschen Sie dies, so verarbeiten wir die beim Login abgefragten personenbezogenen Daten. Beim späteren Einloggen werden nur Ihre Email-Adresse und das von Ihnen gewählte Passwort benötigt.

Für die Neuregistrierung erheben wir Name, Email-Adresse sowie ein von Ihnen gewähltes Passwort.

Erst nach erfolgter Registrierung speichern wir die von Ihnen übermittelten Daten dauerhaft in unserem System.

Wir möchten darauf hinweisen, dass Leitern von Kursen innerhalb unserer Anwendung möglich ist, die Zugangsdaten der Kursteilnehmer für kurze Zeit (maximal 30 Minuten) Mittels eines sog. OTP ("One time password", Einmal-Passwort) ausser Kraft zu setzen. Dies geschieht, um Zugang zu gewähren, wenn der Kursteilnehmer seine persönlichen Zugangdaten nicht vorliegen hat.

Sie können ein einmal angelegtes Nutzerkonto jederzeit von uns löschen lassen. Eine Mitteilung in Textform an die unter Ziffer 1 genannten Kontaktdaten (z.B. E-Mail, Brief) reicht hierfür aus. Wir werden dann Ihre gespeicherten personenbezogenen Daten löschen, soweit wir diese nicht noch aufgrund gesetzlicher Aufbewahrungspflichten speichern müssen.

Rechtgrundlage für die Verarbeitung dieser Daten ist Ihre Einwilligung gemäß Art. 6 Abs. 1 S. 1 a) DSGVO.

### 3.4) Email-Kontakt

Wenn Sie mit uns in Kontakt treten (z. B. per Kontaktformular oder E-Mail), verarbeiten wir Ihre Angaben zur Bearbeitung der Anfrage sowie für den Fall, dass Anschlussfragen entstehen.

Weitere personenbezogene Daten verarbeiten wir nur, wenn Sie dazu einwilligen (Art. 6 Abs. 1 S. 1 a) DSGVO) oder wir ein berechtigtes Interesse an der Verarbeitung Ihrer Daten haben (Art. 6 Abs. 1 S. 1 f) DSGVO). Ein berechtigtes Interesse liegt z. B. darin, auf Ihre E-Mail zu antworten.

### 4) Speicherdauer

Wir speichern Ihre Daten nur so lange, wie Sie es angeben oder wie es zur Erfüllung Ihrer Anfragen notwendig ist.

### 5) Ihre Rechte als von der Datenverarbeitung Betroffener

Nach den anwendbaren Gesetzen haben Sie verschiedene Rechte bezüglich Ihrer personenbezogenen Daten. Möchten Sie diese Rechte geltend machen, so richten Sie Ihre Anfrage bitte per E-Mail oder per Post unter eindeutiger Identifizierung Ihrer Person an die in Ziffer 1 genannte Adresse.

#### 5.1) Widerruf Ihrer Einwilligung zur Datenverarbeitung

Nur mit Ihrer ausdrücklichen Einwilligung sind einige Vorgänge der Datenverarbeitung möglich. Ein Widerruf Ihrer bereits erteilten Einwilligung ist jederzeit möglich. Für den Widerruf genügt eine formlose Mitteilung per E-Mail. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung bleibt vom Widerruf unberührt.

#### 5.2) Recht auf Beschwerde bei der zuständigen Aufsichtsbehörde

Als Betroffener steht Ihnen im Falle eines datenschutzrechtlichen Verstoßes ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Zuständige Aufsichtsbehörde bezüglich datenschutzrechtlicher Fragen ist der Landesdatenschutzbeauftragte des Bundeslandes, in dem sich der Sitz unseres Unternehmens befindet. Dies ist

    Der Hamburgische Beauftragte für Datenschutz und Informationsfreiheit
    Prof. Dr. Johannes Caspar

    Ludwig-Erhard-Str. 22 7.OG
    20459 Hamburg

    Telefon: 040/428 54-40 40
    Telefax: 040/428 54-40 00

    E-Mail: mailbox@datenschutz.hamburg.de

#### 5.3) Recht auf Datenübertragbarkeit

Ihnen steht das Recht zu, Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erfüllung eines Vertrags automatisiert verarbeiten, an sich oder an Dritte aushändigen zu lassen. Die Bereitstellung erfolgt in einem maschinenlesbaren Format. Sofern Sie die direkte Übertragung der Daten an einen anderen Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.

#### 5.4) Recht auf Auskunft, Berichtigung, Sperrung, Löschung

Sie haben jederzeit im Rahmen der geltenden gesetzlichen Bestimmungen das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, Herkunft der Daten, deren Empfänger und den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung, Sperrung oder Löschung dieser Daten. Diesbezüglich und auch zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit über die im Impressum oder unter Nummer 1) aufgeführten Kontaktmöglichkeiten an uns wenden.

### 6) Datensicherheit

Wir sind um die Sicherheit Ihrer Daten maximal bemüht.

Ihre persönlichen Daten werden bei uns verschlüsselt übertragen. Dies gilt für Ihre Testergebnisse und -antworten und auch für den Login. Wir nutzen das Codierungssystem SSL (Secure Socket Layer), weisen jedoch darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.

Zur Sicherung Ihrer Daten unterhalten wir technische und organisatorische Sicherungsmaßnahmen entsprechend Art. 32 DSGVO, die wir immer wieder dem Stand der Technik anpassen.

Wir gewährleisten außerdem nicht, dass unser Angebot zu bestimmten Zeiten zur Verfügung steht; Störungen, Unterbrechungen oder Ausfälle können nicht ausgeschlossen werden. Die von uns verwendeten Server werden regelmäßig sorgfältig gesichert.

### 7) Weitergabe von Daten an Dritte

Grundsätzlich verwenden wir Ihre personenbezogenen Daten nur innerhalb unserer Anwendung.

Jedoch ist zu Beachten, dass die Leiter von Kursen innerhalb unserer Anwendung zugriff auf die Testergebnisse der einzelnen Kursteilnehmer, des gesamten Kurses sowie der gesamten Organisation (z. B. Schule oder Universität) haben.

Eine Datenübertragung an Stellen oder Personen außerhalb der EU findet nicht statt und ist nicht geplant.