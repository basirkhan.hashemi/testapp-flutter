import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;
import 'style.dart';

class ExerciseSolution extends StatefulWidget {
  final Map exercise;

  const ExerciseSolution({Key key, this.exercise}) : super(key: key);

  @override
  _ExerciseSolutionState createState() => _ExerciseSolutionState();
}

class _ExerciseSolutionState extends State<ExerciseSolution> {
  Map<int, Map<String, String>> topicNames = {};
  bool _topicsLoaded = false;

  @override
  void initState() {
    globals.api.call('listTopics', context: context).then((data) {
      List rawTopics = data['response'];
      rawTopics.forEach((currentTopic) {
        // For name and id
        topicNames[int.parse(currentTopic['id'].toString())] = {
          'name': currentTopic['name'],
          'subject': currentTopic['subject']
        };
      });
      setState(() => _topicsLoaded = true);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget answerContainer;
    List answers = [];

    for (int i = 1; i < 6; i++) {
      if (widget.exercise["aw$i"] != "") answers.add(widget.exercise["aw$i"]);
    }
    switch (widget.exercise['awtype']) {
      case "checkbox":
      case "radio":
        {
          answerContainer = ListView.separated(
            shrinkWrap: true,
            primary: false,
            physics: NeverScrollableScrollPhysics(),
            itemCount: answers.length,
            itemBuilder: (context, int n) {
              return (ListTile(
                title: TestAppTex(answers[n]),
                trailing: (jsonDecode(widget.exercise['correct'])
                        .contains(answers[n]))
                    ? FaIcon(
                        FontAwesomeIcons.checkCircle,
                        color: Colors.green,
                      )
                    : null,
              ));
            },
            separatorBuilder: (c, i) => Divider(),
          );
        }
        break;
      case "regex":
      case "text":
      case "number":
        {
          answerContainer = ListTile(
            title: Text(answers[0]),
            trailing: FaIcon(
              FontAwesomeIcons.checkCircle,
              color: Colors.green,
            ),
          );
        }
        break;
      case "fillInText":
      case "fillInRegEx":
      case "inputMultiple":
      case "multipleRegEx":
        {
          answerContainer = ListView.builder(
              shrinkWrap: true,
              primary: false,
              physics: NeverScrollableScrollPhysics(),
              itemCount: answers.length,
              itemBuilder: (context, int n) {
                return (ListTile(
                  title: Text(answers[n]),
                  trailing: FaIcon(
                    FontAwesomeIcons.checkCircle,
                    color: Colors.green,
                  ),
                ));
              });
        }
        break;
    }
    return ResponsiveDrawerScaffold(
        helpPage: HelpPage(
            path: HelpPagePath.randomTest,
            label: S.of(context).helpWithExerciseSolutions),
        body: ListView(children: [
          Hero(
              tag: 'ExerciseCard' + widget.exercise['id'].toString(),
              child: TestAppCard(
                children: [
                  Text(
                    widget.exercise['name'],
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  TestAppTex(widget.exercise['content']),
                  if (widget.exercise['img'] != null &&
                      widget.exercise['img'] != "")
                    Container(
                        child: Image.memory(
                      Uri.parse(widget.exercise['img']).data.contentAsBytes(),
                      scale: 2,
                    )),
                  answerContainer
                ],
              )),
          if (widget.exercise['explanation'] != '')
            TestAppCard(
              children: [
                Text(
                  S.of(context).stepbystepSolution,
                  style: Theme.of(context).textTheme.headline6,
                ),
                TestAppTex(widget.exercise['explanation'])
              ],
            ),
          TestAppCard(
            children: [
              Text(S.of(context).aboutThisExercise,
                  style: Theme.of(context).textTheme.headline6),
              Builder(
                builder: (context) => ListTile(
                  leading: FaIcon(FontAwesomeIcons.hashtag),
                  title: Text(S.of(context).exerciseNo +
                      ' ' +
                      widget.exercise['id'].toString()),
                  onTap: () {
                    Clipboard.setData(
                        ClipboardData(text: widget.exercise['id'].toString()));
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(S.of(context).copiedIdToClipboard)));
                  },
                  subtitle: Text(S.of(context).tapToCopy),
                ),
              ),
              (!_topicsLoaded)
                  ? CenterProgress()
                  : ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (c, i) {
                        int topicId = int.parse(
                            jsonDecode(widget.exercise['topic'])[i].toString());
                        return ListTile(
                            title: Text(
                                '${topicNames[topicId]['subject']} - ${topicNames[topicId]['name']}'));
                      },
                      itemCount: jsonDecode(widget.exercise['topic']).length,
                    ),
              Wrap(
                direction: Axis.horizontal,
                alignment: WrapAlignment.start,
                crossAxisAlignment: WrapCrossAlignment.center,
                spacing: 4,
                runSpacing: 4,
                children: List.generate(
                    jsonDecode(widget.exercise['tags']).length,
                    (index) => Chip(
                          label:
                              Text(jsonDecode(widget.exercise['tags'])[index]),
                        )),
              ),
            ],
          )
        ]));
  }
}
