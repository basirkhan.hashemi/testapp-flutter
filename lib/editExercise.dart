import 'dart:async';
import 'dart:convert';

import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/exercises.dart';
import 'package:test_app_flutter/style.dart';
import 'package:url_launcher/url_launcher.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class EditExercise extends StatefulWidget {
  final int id;

  EditExercise({this.id});

  @override
  _EditExerciseState createState() => _EditExerciseState();
}

class _EditExerciseState extends State<EditExercise>
    with WidgetsBindingObserver {
  bool _loading = true;
  Map exerciseData;

  bool _exerciseActivated = true;
  List _answers = [];
  String _awType = 'multipleChoise';
  Set _selectedTopics = Set();

  Widget _image = Container();

  TextEditingController _nameController = TextEditingController(),
      _contentController = TextEditingController();

  List<TextEditingController> _answerController =
      List.generate(5, (i) => TextEditingController());
  List<TextEditingController> _answerRegExHumanReadableController =
      List.generate(5, (i) => TextEditingController());
  bool _answerOrderMatters = false;
  List<int> _selectedCorrectAnswers = [];

  bool _activationLoading = false;
  DateTime _exerciseDisactivationDate = DateTime.now();

  Completer<Set> _previousTopicsCompleter = Completer();
  ScrollController _scrollController = ScrollController();

  List tags = [];

  TextEditingController _tagsController = TextEditingController();
  TextEditingController _explanationController = TextEditingController();

  @override
  void initState() {
    // if not a new exercise
    if (widget.id != null) {
      globals.api
          .call('fetchExercises',
              options: {'exercise': widget.id, 'images': true},
              context: context)
          .then((data) {
        setState(() {
          exerciseData = data['response'][0];
          _exerciseActivated =
              (int.parse(exerciseData['deactivate'].toString()) <=
                  DateTime.now().millisecondsSinceEpoch / 1000);
          if (!_exerciseActivated)
            _exerciseDisactivationDate = DateTime.fromMillisecondsSinceEpoch(
                int.parse(exerciseData['deactivate'] + '000'));

          for (int i = 1; i < 6; i++) {
            if (exerciseData["aw$i"] != "") _answers.add(exerciseData["aw$i"]);
          }

          tags = jsonDecode(exerciseData['tags']);
          _explanationController.text = exerciseData['explanation'];

          //Exercise type
          switch (exerciseData['awtype']) {
            case 'number':
            case 'text':
            case 'inputMultiple':
            case 'fillInText':
              _awType = 'fillInText';
              break;
            case 'radio':
            case 'checkbox':
              _awType = 'multipleChoise';
              List correctAnswers = jsonDecode(exerciseData['correct']);
              for (int i = 0; i < _answers.length; i++) {
                if (correctAnswers.contains(_answers[i]))
                  _selectedCorrectAnswers.add(i);
              }
              break;
            case 'regex':
            case 'multipleRegEx':
            case 'fillRegEx':
              _awType = 'fillInRegEx';
              // Adding regex' to their controllers
              List regex = jsonDecode(exerciseData['correct']);
              for (int i = 0; i < regex.length; i++) {
                _answerRegExHumanReadableController[i].text = regex[i];
              }
              break;
          }
          if (['fillRegEx', 'fillInText'].contains(exerciseData['awtype']))
            _answerOrderMatters = true;

          // Adding answers to their controllers
          for (int i = 0; i < _answers.length; i++) {
            _answerController[i].text = _answers[i];
          }

          _nameController.text = exerciseData['name'];
          _contentController.text = exerciseData['content'];

          _selectedTopics = Set.from(jsonDecode(exerciseData['topic']));
          _previousTopicsCompleter.complete(_selectedTopics);

          if (exerciseData['img'] != null && exerciseData['img'] != "") {
            _setImageToDataUri(exerciseData['img']);
          } else {
            _image = Container();
          }

          _loading = false;
        });
      });
    } else {
      _loading = false;
    }
    super.initState();
  }

  @override
  void didChangePlatformBrightness() {
    print(WidgetsBinding.instance.window
        .platformBrightness); // should print Brightness.light / Brightness.dark when you switch
    super.didChangePlatformBrightness(); // make sure you call this
  }

  @override
  Widget build(BuildContext context) {
    // Answer list
    Widget _answerList;

    switch (_awType) {
      case 'fillInText':
        _answerList = ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            primary: false,
            itemBuilder: (c, index) {
              if (index == 0)
                return (SwitchListTile(
                  value: _answerOrderMatters,
                  title: Text(S.of(context).answerOrderMatters),
                  onChanged: (selected) => setState(() {
                    _answerOrderMatters = selected;
                  }),
                ));
              return (ListTile(
                title: TextField(
                  controller: _answerController[index - 1],
                  decoration: InputDecoration(
                      labelText: S.of(context).answer + " $index"),
                ),
              ));
            },
            separatorBuilder: (c, i) => Divider(),
            itemCount: 5 + 1);
        break;
      case 'multipleChoise':
        _answerList = ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            primary: false,
            itemBuilder: (c, index) {
              return (CheckboxListTile(
                value: _selectedCorrectAnswers.contains(index),
                onChanged: (selected) {
                  setState(() {
                    (selected)
                        ? _selectedCorrectAnswers.add(index)
                        : _selectedCorrectAnswers.remove(index);
                  });
                },
                title: TextField(
                  controller: _answerController[index],
                  decoration: InputDecoration(
                      labelText:
                          S.of(context).multiplechoiseOption + " ${index + 1}"),
                ),
              ));
            },
            separatorBuilder: (c, i) => Divider(),
            itemCount: 5);
        break;
      case 'fillInRegEx':
        _answerList = ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            primary: false,
            itemBuilder: (c, index) {
              if (index == 0)
                return ListTile(
                  leading: FaIcon(
                    FontAwesomeIcons.infoCircle,
                    color: Colors.orange,
                  ),
                  title: Text(
                      S.of(context).noteRegexAreForAdvancedUsersOnlyDoNotUse),
                );
              if (index == 1)
                return (SwitchListTile(
                  value: _answerOrderMatters,
                  title: Text(S.of(context).answerOrderMatters),
                  onChanged: (selected) => setState(() {
                    _answerOrderMatters = selected;
                  }),
                ));
              if (index == 2)
                return (ListTile(
                  title: RichText(
                      text: TextSpan(
                    style: TextStyle(color: Colors.black),
                    children: [
                      TextSpan(
                        text: S.of(context).important,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                          text: S
                              .of(context)
                              .testYourRegexBeforeSavingWeRecommendTheOpensourceTool),
                    ],
                  )),
                  trailing: OutlinedButton(
                    onPressed: () => launch('https://regex101.com/'),
                    child: Text('regex101'),
                  ),
                ));
              return (ListTile(
                title: Wrap(spacing: 18, runSpacing: 16, children: [
                  Container(
                    constraints: BoxConstraints(maxWidth: 256),
                    child: TextField(
                      controller: _answerController[index - 3],
                      decoration: InputDecoration(
                          labelText: S.of(context).regex + " ${index - 2}",
                          helperText: S.of(context).noDelimiterRequired),
                    ),
                  ),
                  Container(
                    constraints: BoxConstraints(maxWidth: 256),
                    child: TextField(
                      controller:
                          _answerRegExHumanReadableController[index - 3],
                      decoration: InputDecoration(
                          labelText: S.of(context).answer + " ${index - 2}",
                          helperText: S.of(context).humanreadableVersion),
                    ),
                  ),
                ]),
              ));
            },
            separatorBuilder: (c, i) => Divider(),
            itemCount: 5 + 3);
        break;
    }
    return (ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          path: HelpPagePath.editExercise,
          label: S.of(context).helpEditingExercises),
      title: S.of(context).editExericse,
      appBarLeading: IconButton(
        icon: FaIcon(FontAwesomeIcons.times),
        onPressed: () => Navigator.of(context).pop(),
        tooltip: S.of(context).close,
      ),
      appBarActions: (!_loading && widget.id != null)
          ? [
              IconButton(
                icon: FaIcon(FontAwesomeIcons.trash),
                onPressed: deleteExercise,
                tooltip: S.of(context).deleteThisExercise,
              ),
            ]
          : [],
      body: (!_loading)
          ? TestAppScrollBar(
              controller: _scrollController,
              child: ListView(controller: _scrollController, children: [
                TestAppCard(
                  children: <Widget>[
                    Text(S.of(context).exercise,
                        style: Theme.of(context).textTheme.headline6),
                    TextField(
                      controller: _nameController,
                      decoration: InputDecoration(
                          labelText: S.of(context).exerciseName),
                    ),
                    /*Text(
                        'Exercise content: You can write text instructions, use our formula editor or create music scores.'),
                    Container(child: Column(
                      children: [
                        PreferredSize(child: TabPageSelector(controller: _exerciseContentController,), preferredSize: null)
                      ],
                    ),
                    ),*/
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                      child: TextField(
                          controller: _contentController,
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          decoration: InputDecoration(
                              labelText: S.of(context).exercise,
                              border: OutlineInputBorder())),
                    )
                  ],
                ),
                TestAppCard(
                  children: <Widget>[
                    Text(
                      S.of(context).answers,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Center(
                      child: CupertinoSegmentedControl(
                        borderColor: Theme.of(context).accentColor,
                        selectedColor: Theme.of(context).accentColor,
                        unselectedColor: Theme.of(context).cardColor,
                        children: {
                          'multipleChoise': Padding(
                            child: Text(S.of(context).multipleChoise),
                            padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                          ),
                          'fillInText': Padding(
                            child: Text(S.of(context).input),
                            padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                          ),
                          'fillInRegEx': Padding(
                            child: Text(S.of(context).inputUsingRegex),
                            padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                          ),
                        },
                        onValueChanged: (newVal) {
                          setState(() {
                            _awType = newVal;
                          });
                        },
                        groupValue: _awType,
                        padding: EdgeInsets.all(8),
                      ),
                    ),
                    Divider(),
                    ListTile(
                        leading: FaIcon(FontAwesomeIcons.infoCircle),
                        title: Text(S
                            .of(context)
                            .pleaseJustLeaveEmptyUnusedTextFields)),
                    _answerList
                  ],
                ),
                TestAppCard(
                  children: [
                    Text(
                      S.of(context).solutionTags,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Text(S
                        .of(context)
                        .youCanProvideAStepbystepSolutionForThisExerciseThis),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                      child: TextField(
                        maxLines: null,
                        controller: _explanationController,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: S.of(context).stepbystepSolution),
                      ),
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                          S.of(context).tagsCanHelpToFindThisExerciseLater),
                    ),
                    Wrap(
                      spacing: 8.0,
                      runSpacing: 4.0,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Container(
                          constraints: BoxConstraints(maxWidth: 320),
                          child: TextField(
                            controller: _tagsController,
                            maxLines: 1,
                            decoration: InputDecoration(
                                border: UnderlineInputBorder(),
                                labelText: S.of(context).addTags,
                                helperText:
                                    S.of(context).egRadiationDerivativeEtc),
                          ),
                        ),
                        OutlinedButton.icon(
                            onPressed: () => setState(() {
                                  tags.add(_tagsController.text);
                                  _tagsController.clear();
                                }),
                            icon: FaIcon(FontAwesomeIcons.plus),
                            label: Text(S.of(context).addTag))
                      ],
                    ),
                    Wrap(
                      direction: Axis.horizontal,
                      alignment: WrapAlignment.start,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      spacing: 4,
                      runSpacing: 4,
                      children: List.generate(
                          tags.length,
                          (index) => Chip(
                                label: Text(tags[index]),
                                deleteButtonTooltipMessage:
                                    S.of(context).delete,
                                deleteIcon: FaIcon(
                                  FontAwesomeIcons.times,
                                  size: 18,
                                ),
                                onDeleted: () =>
                                    setState(() => tags.removeAt(index)),
                              )),
                    ),
                  ],
                ),
                TestAppCard(children: <Widget>[
                  Text(S.of(context).topics,
                      style: Theme.of(context).textTheme.headline6),
                  TopicSelector(
                    onSelect: (newTopics) => {_selectedTopics = newTopics},
                    selectedTopicsFuture: _previousTopicsCompleter.future,
                  ),
                ]),
                (widget.id != null)
                    ? TestAppCard(
                        children: <Widget>[
                          Text(
                            S.of(context).exerciseActivation,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          Text(S.of(context).ifYouPlanToWriteAJoinTestYouCan),
                          (_exerciseActivated)
                              ? Container(
                                  child: ListTile(
                                    leading: (_activationLoading)
                                        ? CircularProgressIndicator()
                                        : FaIcon(FontAwesomeIcons.lockOpen),
                                    onTap: _disableExercise,
                                    title: Text(S
                                        .of(context)
                                        .thisExerciseIsActivatedTapToDisable),
                                  ),
                                )
                              : ListTile(
                                  leading: (_activationLoading)
                                      ? CircularProgressIndicator()
                                      : FaIcon(FontAwesomeIcons.userLock),
                                  onTap: _activateExercise,
                                  title: Text(S
                                          .of(context)
                                          .thisExerciseIsDisabledTill +
                                      ' ${_exerciseDisactivationDate.day.toString() + '.' + _exerciseDisactivationDate.month.toString() + '.' + _exerciseDisactivationDate.year.toString()}. ' +
                                      S.of(context).tapToActivate),
                                )
                        ],
                      )
                    : Container(),
                TestAppCard(
                  children: <Widget>[
                    Text(
                      S.of(context).image,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Text(
                      S
                          .of(context)
                          .noteItIsStrictlyProhibitedToUploadAnyImageDepicting,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    (widget.id != null)
                        ? Wrap(
                            direction: Axis.horizontal,
                            alignment: WrapAlignment.start,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            spacing: 4,
                            runSpacing: 4,
                            children: [
                                ElevatedButton(
                                  onPressed: _uploadImage,
                                  child: Text(S.of(context).uploadImage),
                                ),
                                FloatingActionButton(
                                  heroTag: 'RemoveImage',
                                  mini: true,
                                  onPressed: _removeImage,
                                  child: FaIcon(FontAwesomeIcons.trash),
                                  tooltip: S.of(context).deleteImage,
                                ),
                              ])
                        : Text(S
                            .of(context)
                            .uploadingImagesIsOnlyPossibleAfterCreatingTheExercisePlease),
                    Container(
                      child: _image,
                    )
                  ],
                ),
                TestAppCard(
                  children: <Widget>[
                    Text(
                      S.of(context).iOwnTheCopyrightOfThisExercise,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    Text(S
                        .of(context)
                        .bySavingThisExerciseIAsignAllRightsOfThis),
                    Row(
                      children: <Widget>[
                        OutlinedButton(
                          onPressed: () =>
                              launch(S.of(context).creativeCommonsUrl),
                          child: Row(
                            children: <Widget>[
                              FaIcon(FontAwesomeIcons.creativeCommons),
                              FaIcon(FontAwesomeIcons.creativeCommonsBy),
                              FaIcon(FontAwesomeIcons.creativeCommonsSa),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                )
              ]),
            )
          : CenterProgress(),
      floatingActionButton: (!_loading)
          ? FloatingActionButton.extended(
              onPressed: saveExercise,
              label: Text(S.of(context).saveExercise),
              icon: FaIcon(FontAwesomeIcons.check),
            )
          : FloatingActionButton(
              onPressed: () {},
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            ),
    ));
  }

  Future<void> saveExercise() async {
    Map<String, dynamic> options = Map();
    options['name'] = _nameController.text.trim();
    options['content'] = _contentController.text.trim();

    // Cleaning answers
    List answers = _answerController.map((controller) {
      return (controller.text.trim());
    }).toList();

    // Encoding tags
    options['tags'] = jsonEncode(tags);

    options['explanation'] = _explanationController.text;

    // Checking the correct answers
    switch (_awType) {
      case 'fillInRegEx':
        List regex = _answerRegExHumanReadableController.map((controller) {
          return (controller.text.trim());
        }).toList();
        options['answers'] = regex.where((el) => el != '').toList();

        if (_answerOrderMatters) {
          options['type'] = 'fillInRegEx';
        } else {
          options['type'] = 'multipleRegEx';
          answers.sort();
        }

        options['correct'] = answers.where((el) => el != '').toList();

        break;

      case 'fillInText':
        options['correct'] = answers.where((el) => el != '').toList();

        if (_answerOrderMatters) {
          options['type'] = 'fillInText';
        } else {
          options['type'] = 'inputMultiple';
          answers.sort();
        }

        options['answers'] = answers.where((el) => el != '').toList();

        break;

      case 'multipleChoise':
        List correct = [];
        _selectedCorrectAnswers.forEach((correctAnswerId) {
          // Using controller instead of existing answer List because there may be empty answers
          correct.add(_answerController[correctAnswerId].text.trim());
        });

        if (correct.length == 1) {
          options['type'] = 'radio';
        } else {
          options['type'] = 'checkbox';
        }

        options['answers'] = answers.where((el) => el != '').toList();
        options['correct'] = correct;

        break;
    }

    // Adding topics
    options['topics'] = _selectedTopics.toList();

    if (!(_selectedTopics.isNotEmpty &&
        options['name'].isNotEmpty &&
        options['content'].isNotEmpty &&
        options['answers'].isNotEmpty &&
        options['correct'].isNotEmpty &&
        options['topics'].isNotEmpty)) {
      /*Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Please fill in all required fields.'),
      ));*/
      _loading = false;
      return;
    }

    String job = 'addExercise';
    if (widget.id != null) {
      options['exercise'] = widget.id;
      job = 'updateExercise';
    }

    globals.api.call(job, options: options, context: context).then((data) {
      setState(() {
        _loading = false;
      });
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (b) => Exercises()));
    });
    setState(() {
      _loading = true;
    });
  }

  void deleteExercise() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Text(S.of(context).areYouSureToDeleteThisExercise),
              actions: <Widget>[
                MaterialButton(
                  onPressed: Navigator.of(context).pop,
                  child: Text(S.of(context).cancel),
                ),
                MaterialButton(
                    onPressed: () async {
                      setState(() {
                        _loading = true;
                      });
                      globals.api
                          .call('deleteExercise',
                              options: {'exercise': widget.id},
                              context: context)
                          .then((data) {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (b) => Exercises()));
                      });
                    },
                    child: Text(S.of(context).yes)),
              ],
            ));
  }

  void _disableExercise() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now().add(Duration(days: 3)),
            firstDate: DateTime.now(),
            lastDate: DateTime.now().add(Duration(days: 14)))
        .then((selectedDate) async {
      if (selectedDate == null) return;
      _exerciseDisactivationDate = selectedDate;
      var timestamp = selectedDate.millisecondsSinceEpoch.toString().substring(
          0, selectedDate.millisecondsSinceEpoch.toString().length - 3);

      setState(() {
        _activationLoading = true;
      });
      globals.api
          .call('exerciseActivation',
              options: {
                "exercise": exerciseData['id'],
                'activation': timestamp
              },
              context: context)
          .then((data) {
        if (data['response']) {
          _exerciseActivated = !_exerciseActivated;
        }
        setState(() {
          _activationLoading = false;
        });
      });
    });
  }

  Future<void> _activateExercise() async {
    setState(() {
      _activationLoading = true;
    });
    globals.api
        .call('exerciseActivation',
            options: {"exercise": exerciseData['id'], 'activation': 0},
            context: context)
        .then((data) {
      if (data['response']) {
        _exerciseActivated = !_exerciseActivated;
      }
      setState(() {
        _activationLoading = false;
      });
    });
  }

  void _uploadImage() async {
    FilePickerCross filePicker =
        await FilePickerCross.importFromStorage(type: FileTypeCross.image);

    String base64Image = filePicker.toBase64();
    String dataUrl =
        'data:image/png;base64,' + base64Image; // image/png always w
    setState(() {
      _setImageToDataUri(dataUrl);
    });
    globals.api
        .uploadImage(widget.id, filePicker.toMultipartFile(filename: 'upload'))
        .then((v) {});
  }

  Future<void> _removeImage() async {
    globals.api
        .call('removeImage', options: {'exercise': widget.id}, context: context)
        .then((response) {
      setState(() {
        _image = Container();
      });
    });
  }

  void _setImageToDataUri(String imageString) {
    final UriData data = Uri.parse(imageString).data;
    _image = Container(
        child: Image.memory(
      data.contentAsBytes(),
      scale: 2,
    ));
  }
}

class TopicSelector extends StatefulWidget {
  final Function onSelect;
  final Future<Set> selectedTopicsFuture;

  TopicSelector({Key key, this.onSelect, this.selectedTopicsFuture})
      : super(key: key);

  @override
  _TopicSelectorState createState() => _TopicSelectorState();
}

class _TopicSelectorState extends State<TopicSelector>
    with AutomaticKeepAliveClientMixin<TopicSelector> {
  bool _topicsLoaded = false;
  Set _selectedTopics = Set();
  Map<int, String> topicNames = {};
  Map<int, String> topicGrades = {};
  Map<int, String> topicSubjects = {};
  Map topics;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    if (widget.selectedTopicsFuture != null)
      widget.selectedTopicsFuture.then((Set previouslySelectedTopics) {
        _selectedTopics = previouslySelectedTopics;
        try {
          setState(() {});
        } catch (e) {}
      });
    _refreshTopicList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    List<DropdownMenuItem> dropdownItems;
    dropdownItems = topicNames.keys.map<DropdownMenuItem<int>>((key) {
      return DropdownMenuItem<int>(
        value: key,
        child: Text(
          topicSubjects[key] +
              ' ' +
              S.of(context).g +
              topicGrades[key].toString() +
              ': ' +
              topicNames[key],
          style: Theme.of(context).textTheme.bodyText1,
        ),
      );
    }).toList()
      ..sort(
          (a, b) => (a.child as Text).data.compareTo((b.child as Text).data));
    return (_topicsLoaded)
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Wrap(spacing: 8.0, runSpacing: 4.0, children: [
                DropdownButton<int>(
                  iconSize: 24,
                  icon: FaIcon(FontAwesomeIcons.angleDown),
                  elevation: 16,
                  hint: Text(S.of(context).addANewTopic),
                  style: TextStyle(color: Colors.lightBlue),
                  underline: Container(
                    height: 2,
                    color: Colors.green,
                  ),
                  onChanged: (int newValue) {
                    setState(() {
                      _selectedTopics.add(newValue);
                    });
                    widget.onSelect(_selectedTopics);
                  },
                  items: dropdownItems,
                ),
                FloatingActionButton(
                  heroTag: 'addTopicFAB',
                  // Preventing crashes for animations when navigating
                  onPressed: _createNewTopic,
                  tooltip: S.of(context).createNewTopic,
                  child: FaIcon(FontAwesomeIcons.plus),
                  elevation: 2,
                  mini: true,
                )
              ]),
              Wrap(
                spacing: 8.0, // gap between adjacent chips
                runSpacing: 4.0, // gap between lines
                children: _selectedTopics.map((topicId) {
                  return (Chip(
                      deleteIcon: FaIcon(
                        FontAwesomeIcons.times,
                        size: 18,
                      ),
                      deleteButtonTooltipMessage: S.of(context).delete,
                      onDeleted: () {
                        setState(() {
                          _selectedTopics.remove(int.parse(topicId.toString()));
                        });
                        widget.onSelect(_selectedTopics);
                      },
                      label: Text(S.of(context).g +
                          topicGrades[int.parse(topicId.toString())]
                              .toString() +
                          ': ' +
                          topicNames[int.parse(topicId.toString())])));
                }).toList(),
              ),
            ],
          )
        : CenterProgress();
  }

  void _createNewTopic() {
    showDialog(context: context, builder: (c) => AddTopicDialog())
        .then((apiOptions) async {
      if (apiOptions == null) return;
      setState(() {
        _topicsLoaded = false;
      });
      globals.api
          .call('addTopic', options: apiOptions, context: context)
          .then((value) {
        _refreshTopicList();
      });
    });
  }

  Future<void> _refreshTopicList() async {
    globals.api.call('listTopics', context: context).then((data) {
      List rawTopics = data['response'];
      Map topicsBySubjectNames = Map();
      rawTopics.forEach((currentTopic) {
        if (!topicsBySubjectNames.keys.contains(currentTopic['subject']))
          topicsBySubjectNames[currentTopic['subject']] = [];
        topicsBySubjectNames[currentTopic['subject']].add(currentTopic);
        // For name and id
        topicNames[int.parse(currentTopic['id'].toString())] =
            currentTopic['name'];
        topicGrades[int.parse(currentTopic['id'].toString())] =
            currentTopic['year'];
        topicSubjects[int.parse(currentTopic['id'].toString())] =
            currentTopic['subject'];
      });
      setState(() {
        _topicsLoaded = true;
        topics = topicsBySubjectNames;
      });
    });
  }
}

class AddTopicDialog extends StatefulWidget {
  @override
  _AddTopicDialogState createState() => _AddTopicDialogState();
}

class _AddTopicDialogState extends State<AddTopicDialog> {
  TextEditingController _addTopicName = TextEditingController();
  TextEditingController _addTopicSubject = TextEditingController();
  int grade = 0;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        S.of(context).createNewTopic,
      ),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextField(
            controller: _addTopicName,
            decoration: InputDecoration(labelText: S.of(context).topicName),
          ),
          TextField(
            controller: _addTopicSubject,
            decoration: InputDecoration(labelText: S.of(context).subject),
          ),
          GradeSlider(onChanged: (newGrade) => grade = newGrade),
        ],
      ),
      actions: <Widget>[
        MaterialButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(S.of(context).cancel),
        ),
        MaterialButton(
          onPressed: () => setState(() => Navigator.of(context).pop({
                'name': _addTopicName.text,
                'subject': _addTopicSubject.text,
                'year': grade
              })),
          child: Text(S.of(context).create),
        )
      ],
    );
  }
}

class GradeSlider extends StatefulWidget {
  final Function(int newGrade) onChanged;
  final int initialValue;

  const GradeSlider({Key key, this.onChanged, this.initialValue})
      : super(key: key);

  @override
  _GradeSliderState createState() => _GradeSliderState();
}

class _GradeSliderState extends State<GradeSlider> {
  int grade = 0;

  @override
  void initState() {
    grade = widget.initialValue ?? 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          S.of(context).grade,
          style: Theme.of(context).textTheme.headline6,
        ),
        Slider(
            value: grade.toDouble(),
            onChanged: (newValue) => setState(() => grade = newValue.round()),
            onChangeEnd: (newValue) => widget.onChanged(grade),
            label: S.of(context).grade,
            min: 0,
            max: 13,
            semanticFormatterCallback: (double newValue) =>
                S.of(context).assignToGrade + '${newValue.round()}'),
        Text((grade != 0)
            ? S.of(context).assignToGrade + '$grade.'
            : S.of(context).doNotAssignToAnyGrade)
      ],
    );
  }
}
