import 'package:app_review/app_review.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/about.dart';
import 'package:test_app_flutter/api.dart';
import 'package:test_app_flutter/classes.dart';
import 'package:test_app_flutter/exercises.dart';
import 'package:test_app_flutter/gdpr.dart';
import 'package:test_app_flutter/joinTest.dart';
import 'package:test_app_flutter/practise.dart';
import 'package:test_app_flutter/settings.dart';
import 'package:test_app_flutter/style.dart';
import 'package:url_launcher/url_launcher.dart';

import 'assignments.dart';
import 'dashboard.dart';
import 'exerciseSets.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;
import 'schoolManagement.dart';
import 'userScore.dart';

class TestAppDrawer extends StatefulWidget {
  final HelpPage groupValue;

  const TestAppDrawer({Key key, this.groupValue}) : super(key: key);

  @override
  _TestAppDrawerState createState() => _TestAppDrawerState();
}

class _TestAppDrawerState extends State<TestAppDrawer>
    with WidgetsBindingObserver {
  bool _isAdmin = false;
  HelpPagePath _groupValue;
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    _isAdmin = globals.isAdmin;
    globals.api.initializationDone.then(checkAdmin);
    if (widget.groupValue != null) _groupValue = widget.groupValue.path;
    super.initState();
  }

  void checkAdmin(param) {
    Preferences().fetch('Admin').then((adminString) {
      setState(() {
        _isAdmin = adminString == 'true';
      });
    });
  }

  @override
  void didChangePlatformBrightness() {
    print(WidgetsBinding.instance.window
        .platformBrightness); // should print Brightness.light / Brightness.dark when you switch
    super.didChangePlatformBrightness(); // make sure you call this
  }

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'MainDrawer',
      child: Drawer(
          child: Container(
        color: Theme.of(context).cardColor,
        child: TestAppScrollBar(
          controller: _scrollController,
          child: ListView(
            controller: _scrollController,
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              Semantics(
                hidden: true,
                child: UserAccountsDrawerHeader(
                  accountName: Text(
                    "TestApp",
                    style: Theme.of(context).textTheme.headline3,
                  ),
                  accountEmail: Text(S.of(context).slogan),
                  currentAccountPicture:
                      Image.asset("assets/bear-standard.png"),
                  decoration: BoxDecoration(color: Colors.lightBlue),
                ),
              ),
              DrawerListTile(
                groupValue: _groupValue,
                helpPagePaths: [HelpPagePath.dashboard],
                icon: FaIcon(FontAwesomeIcons.home),
                title: S.of(context).dashboard,
                builder: (context) => Dashboard(),
              ),
              if (_isAdmin)
                Text(
                  S.of(context).forTeachers,
                  style: Theme.of(context).textTheme.subtitle2,
                ),
              if (_isAdmin) Divider(),
              if (_isAdmin)
                DrawerListTile(
                    groupValue: _groupValue,
                    helpPagePaths: [
                      HelpPagePath.exercises,
                      HelpPagePath.editExercise,
                      HelpPagePath.stackEdit
                    ],
                    icon: FaIcon(FontAwesomeIcons.graduationCap),
                    title: S.of(context).exercises,
                    builder: (context) => Exercises()),
              if (_isAdmin)
                DrawerListTile(
                    groupValue: _groupValue,
                    helpPagePaths: [
                      HelpPagePath.classes,
                      HelpPagePath.classScore,
                      HelpPagePath.editClass
                    ],
                    icon: FaIcon(FontAwesomeIcons.users),
                    title: S.of(context).classes,
                    builder: (context) => Classes()),
              if (_isAdmin)
                DrawerListTile(
                    groupValue: _groupValue,
                    helpPagePaths: [
                      HelpPagePath.joinTests,
                      HelpPagePath.writeJoinTest,
                      HelpPagePath.editJoinTest
                    ],
                    icon: FaIcon(FontAwesomeIcons.boxOpen),
                    title: S.of(context).joinTests,
                    builder: (context) => ExerciseSets()),
              if (_isAdmin)
                DrawerListTile(
                    groupValue: _groupValue,
                    helpPagePaths: [HelpPagePath.schoolManagement],
                    icon: FaIcon(FontAwesomeIcons.school),
                    title: S.of(context).schoolManagement,
                    builder: (context) => SchoolManagement()),
              Text(
                S.of(context).forStudents,
                style: Theme.of(context).textTheme.subtitle2,
              ),
              Divider(),
              DrawerListTile(
                  groupValue: _groupValue,
                  helpPagePaths: [
                    HelpPagePath.randomTest,
                    HelpPagePath.writeTest
                  ],
                  icon: FaIcon(FontAwesomeIcons.dumbbell),
                  title: S.of(context).practise,
                  builder: (context) => Practise()),
              DrawerListTile(
                  groupValue: _groupValue,
                  helpPagePaths: [HelpPagePath.joinTest],
                  icon: FaIcon(FontAwesomeIcons.chalkboardTeacher),
                  title: S.of(context).joinTest,
                  builder: (context) => JoinTest()),
              DrawerListTile(
                  groupValue: _groupValue,
                  helpPagePaths: [HelpPagePath.assignments],
                  icon: FaIcon(FontAwesomeIcons.calendarCheck),
                  title: S.of(context).assignments,
                  builder: (context) => Assignments()),
              DrawerListTile(
                  groupValue: _groupValue,
                  helpPagePaths: [
                    HelpPagePath.yourScore,
                    HelpPagePath.testScore
                  ],
                  icon: FaIcon(FontAwesomeIcons.chartLine),
                  title: S.of(context).yourScore,
                  builder: (context) => UserScore()),
              Text(
                S.of(context).general,
                style: Theme.of(context).textTheme.subtitle2,
              ),
              Divider(),
              DrawerListTile(
                  groupValue: _groupValue,
                  helpPagePaths: [HelpPagePath.about],
                  icon: FaIcon(FontAwesomeIcons.info),
                  title: S.of(context).about,
                  builder: (context) => About()),
              DrawerListTile(
                  groupValue: _groupValue,
                  helpPagePaths: [HelpPagePath.settings],
                  icon: FaIcon(FontAwesomeIcons.cog),
                  title: S.of(context).settings,
                  builder: (context) => SettingsPage()),
              if (!kIsWeb &&
                  ( //Theme.of(context).platform == TargetPlatform.iOS ||
                      Theme.of(context).platform == TargetPlatform.android))
                Container(
                  color: Theme.of(context).accentColor,
                  child: ListTile(
                    leading: new FaIcon(
                      FontAwesomeIcons.solidStar,
                      color: Colors.white,
                    ),
                    title: new Text(
                      S.of(context).rateTestapp,
                      style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(color: Colors.white),
                    ),
                    onTap: () {
                      try {
                        AppReview.writeReview.then((onValue) {
                          setState(() {
                            print(onValue);
                          });
                          print(onValue);
                        });
                      } catch (e) {
                        print('Error requesting review.');
                      }
                    },
                  ),
                ),
            ],
          ),
        ),
      )),
    );
  }
}

class ResponsiveDrawerScaffold extends StatefulWidget {
  final Widget body;
  final String title;
  final Widget appBarLeading;
  final List<Widget> appBarActions;
  final Widget appBarBottom;
  final FloatingActionButtonLocation floatingActionButtonLocation;
  final FloatingActionButton floatingActionButton;
  final bool showDrawer;
  final HelpPage helpPage;
  final Widget bottomNavigationBar;
  final bool extendedBody;

  ResponsiveDrawerScaffold(
      {this.body,
      this.title = 'TestApp',
      this.appBarLeading,
      this.appBarActions,
      this.appBarBottom,
      this.floatingActionButton,
      this.floatingActionButtonLocation,
      this.showDrawer = true,
      @required this.helpPage,
      this.bottomNavigationBar,
      this.extendedBody = false}) {
    assert(title != '');
  }

  @override
  _ResponsiveDrawerScaffoldState createState() =>
      _ResponsiveDrawerScaffoldState();
}

class _ResponsiveDrawerScaffoldState extends State<ResponsiveDrawerScaffold> {
  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).size.width < 1024) {
      return Scaffold(
        extendBody: true,
        bottomNavigationBar: widget.bottomNavigationBar,
        appBar: AppBar(
          title: Text(widget.title),
          actions: _generateAppBarActions(),
          bottom: widget.appBarBottom,
          leading: (widget.appBarLeading != null)
              ? widget.appBarLeading
              // Lots of code for a simple hamburger menu... Basically, it's just
              // Translating the non-square Hamburger menu icon into a 24x24 square
              // and making the Scaffold.of() accessible.
              : Builder(
                  builder: (context) => IconButton(
                      icon: Transform(
                        transform: Matrix4(
                            1.3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)
                          ..scale(0.8),
                        origin: Offset(12, 12),
                        child: FaIcon(FontAwesomeIcons.bars),
                      ),
                      tooltip: S.of(context).openNavigationMenu,
                      onPressed: () {
                        Scaffold.of(context).openDrawer();
                      })),
        ),
        drawer: (widget.showDrawer)
            ? TestAppDrawer(groupValue: widget.helpPage)
            : null,
        body: Builder(builder: (context) => widget.body),
        floatingActionButton: widget.floatingActionButton,
        floatingActionButtonLocation: widget.floatingActionButtonLocation,
      );
    } else {
      return Row(children: [
        if (widget.showDrawer)
          Material(
            elevation: 20,
            shadowColor: Color(0xffA22447).withOpacity(.05),
            child: Container(
                width: 256,
                child: TestAppDrawer(
                  groupValue: widget.helpPage,
                )),
          ),
        Expanded(
          child: Scaffold(
            extendBody: true,
            bottomNavigationBar: widget.bottomNavigationBar,
            appBar: AppBar(
              title: Text(widget.title),
              actions: _generateAppBarActions(),
              bottom: widget.appBarBottom,
              automaticallyImplyLeading: false,
              leading: widget.appBarLeading,
            ),
            body: Builder(builder: (context) => widget.body),
            floatingActionButton: widget.floatingActionButton,
            floatingActionButtonLocation: widget.floatingActionButtonLocation,
          ),
        )
      ]);
    }
  }

  _generateAppBarActions() {
    List<Widget> icons = [];
    if (widget.appBarActions != null) icons.addAll(widget.appBarActions);
    icons.add(HelpPopup(helpPage: widget.helpPage));
    return icons;
  }
}

class HelpPopup extends StatelessWidget {
  final HelpPage helpPage;
  final bool showGdpr;

  const HelpPopup({Key key, this.helpPage, this.showGdpr = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      tooltip: S.of(context).openHelpMenu,
      icon: FaIcon(FontAwesomeIcons.questionCircle),
      onSelected: (result) {
        if (result == ActionPopups.showGdpr) {
          showSimplifiedGDPR(context);
          return;
        }
        String langCode = (Localizations.localeOf(context).languageCode + '_')
            .substring(0, 3)
            .replaceAll('_', '');
        String url = 'https://docs.testapp.ga/';
        if (result == ActionPopups.generalHelp) langCode += '/';
        if (result == ActionPopups.specificHelp)
          url += _helpUrl[helpPage.path][langCode];
        launch(Uri.encodeFull(url));
      },
      itemBuilder: (BuildContext context) => <PopupMenuEntry<ActionPopups>>[
        PopupMenuItem<ActionPopups>(
          value: ActionPopups.generalHelp,
          child: Text(S.of(context).userManual),
        ),
        if (helpPage != null)
          PopupMenuItem<ActionPopups>(
            value: ActionPopups.specificHelp,
            child: Text(((helpPage != null && helpPage.label != null)
                ? helpPage.label
                : S.of(context).helpForThisPage)),
          ),
        if (showGdpr)
          PopupMenuItem<ActionPopups>(
            value: ActionPopups.showGdpr,
            child: Text(S.of(context).privacyPolicy),
          ),
      ],
    );
  }
}

enum ActionPopups { generalHelp, specificHelp, showGdpr }

class HelpPage {
  final String label;
  final HelpPagePath path;

  HelpPage({this.label, @required this.path});
}

enum HelpPagePath {
  login,
  about,
  register,
  registerTeacher,
  dashboard,
  randomTest,
  joinTest,
  assignments,
  writeTest,
  yourScore,
  testScore,
  exercises,
  editExercise,
  stackEdit,
  classes,
  editClass,
  classScore,
  joinTests,
  editJoinTest,
  writeJoinTest,
  schoolManagement,
  settings
}

Map<HelpPagePath, Map<String, String>> _helpUrl = {
  HelpPagePath.login: {
    'en': 'de/allgemein/registrieren/',
    'de': 'de/allgemein/registrieren/',
    'fr': 'de/allgemein/registrieren/',
    'tlh': 'de/allgemein/registrieren/',
  },
  HelpPagePath.about: {
    'en': 'de/allgemein/about/',
    'de': 'de/allgemein/about/',
    'fr': 'de/allgemein/about/',
    'tlh': 'de/allgemein/about/',
  },
  HelpPagePath.register: {
    'en': 'de/allgemein/registrieren/',
    'de': 'de/allgemein/registrieren/',
    'fr': 'de/allgemein/registrieren/',
    'tlh': 'de/allgemein/registrieren/',
  },
  HelpPagePath.registerTeacher: {
    'en': 'de/lehrer/lehrer-registrieren/',
    'de': 'de/lehrer/lehrer-registrieren/',
    'fr': 'de/lehrer/lehrer-registrieren/',
    'tlh': 'de/lehrer/lehrer-registrieren/',
  },
  HelpPagePath.dashboard: {
    'en': 'de/schüler/dashboard/',
    'de': 'de/schüler/dashboard/',
    'fr': 'de/schüler/dashboard/',
    'tlh': 'de/schüler/dashboard/',
  },
  HelpPagePath.randomTest: {
    'en': 'de/schüler/random-test/',
    'de': 'de/schüler/random-test/',
    'fr': 'de/schüler/random-test/',
    'tlh': 'de/schüler/random-test/',
  },
  HelpPagePath.joinTest: {
    'en': 'de/schüler/join-test/',
    'de': 'de/schüler/join-test/',
    'fr': 'de/schüler/join-test/',
    'tlh': 'de/schüler/join-test/',
  },
  HelpPagePath.assignments: {
    'en': 'de/schüler/hausaufgaben/',
    'de': 'de/schüler/hausaufgaben/',
    'fr': 'de/schüler/hausaufgaben/',
    'tlh': 'de/schüler/hausaufgaben/',
  },
  HelpPagePath.writeTest: {
    'en': 'de/schüler/test-schreiben/',
    'de': 'de/schüler/test-schreiben/',
    'fr': 'de/schüler/test-schreiben/',
    'tlh': 'de/schüler/test-schreiben/',
  },
  HelpPagePath.yourScore: {
    'en': 'de/schüler/score-schüler/',
    'de': 'de/schüler/score-schüler/',
    'fr': 'de/schüler/score-schüler/',
    'tlh': 'de/schüler/score-schüler/',
  },
  HelpPagePath.testScore: {
    'en': 'de/schüler/test-score/',
    'de': 'de/schüler/test-score/',
    'fr': 'de/schüler/test-score/',
    'tlh': 'de/schüler/test-score/',
  },
  HelpPagePath.exercises: {
    'en': 'de/lehrer/aufgaben/',
    'de': 'de/lehrer/aufgaben/',
    'fr': 'de/lehrer/aufgaben/',
    'tlh': 'de/lehrer/aufgaben/',
  },
  HelpPagePath.editExercise: {
    'en': 'de/lehrer/aufgabe-bearbeiten/',
    'de': 'de/lehrer/aufgabe-bearbeiten/',
    'fr': 'de/lehrer/aufgabe-bearbeiten/',
    'tlh': 'de/lehrer/aufgabe-bearbeiten/',
  },
  HelpPagePath.stackEdit: {
    'en': 'de/lehrer/aufgaben-stapelverarbeitung/',
    'de': 'de/lehrer/aufgaben-stapelverarbeitung/',
    'fr': 'de/lehrer/aufgaben-stapelverarbeitung/',
    'tlh': 'de/lehrer/aufgaben-stapelverarbeitung/',
  },
  HelpPagePath.classes: {
    'en': 'de/lehrer/kurse/',
    'de': 'de/lehrer/kurse/',
    'fr': 'de/lehrer/kurse/',
    'tlh': 'de/lehrer/kurse/',
  },
  HelpPagePath.editClass: {
    'en': 'de/lehrer/kurse-bearbeiten/',
    'de': 'de/lehrer/kurse-bearbeiten/',
    'fr': 'de/lehrer/kurse-bearbeiten/',
    'tlh': 'de/lehrer/kurse-bearbeiten/',
  },
  HelpPagePath.classScore: {
    'en': 'de/lehrer/kurs-score/',
    'de': 'de/lehrer/kurs-score/',
    'fr': 'de/lehrer/kurs-score/',
    'tlh': 'de/lehrer/kurs-score/',
  },
  HelpPagePath.joinTests: {
    'en': 'de/lehrer/join-tests/',
    'de': 'de/lehrer/join-tests/',
    'fr': 'de/lehrer/join-tests/',
    'tlh': 'de/lehrer/join-tests/',
  },
  HelpPagePath.editJoinTest: {
    'en': 'de/lehrer/join-test-bearbeiten/',
    'de': 'de/lehrer/join-test-bearbeiten/',
    'fr': 'de/lehrer/join-test-bearbeiten/',
    'tlh': 'de/lehrer/join-test-bearbeiten/',
  },
  HelpPagePath.writeJoinTest: {
    'en': 'de/lehrer/join-test-schreiben/',
    'de': 'de/lehrer/join-test-schreiben/',
    'fr': 'de/lehrer/join-test-schreiben/',
    'tlh': 'de/lehrer/join-test-schreiben/',
  },
  HelpPagePath.schoolManagement: {
    'en': 'de/lehrer/schulverwaltung/',
    'de': 'de/lehrer/schulverwaltung/',
    'fr': 'de/lehrer/schulverwaltung/',
    'tlh': 'de/lehrer/schulverwaltung/',
  },
  HelpPagePath.settings: {
    'en': 'de/allgemein/einstellungen/',
    'de': 'de/allgemein/einstellungen/',
    'fr': 'de/allgemein/einstellungen/',
    'tlh': 'de/allgemein/einstellungen/',
  }
};

class DrawerListTile extends StatelessWidget {
  final Widget icon;
  final String title;
  final Function builder;
  final List<HelpPagePath> helpPagePaths;
  final HelpPagePath groupValue;

  const DrawerListTile({
    Key key,
    this.icon,
    this.title,
    this.builder,
    this.helpPagePaths,
    this.groupValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget drawerTile = ListTile(
      leading: icon,
      title: Text(title),
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: builder),
      ),
    );
    return (helpPagePaths != null &&
            groupValue != null &&
            helpPagePaths.contains(groupValue))
        ? Stack(children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 4, 16, 0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    constraints: BoxConstraints(minHeight: 44),
                    decoration: BoxDecoration(
                        color: Theme.of(context).accentColor.withAlpha(84),
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(24),
                            bottomRight: Radius.circular(24))),
                    //constraints: BoxConstraints.expand(),
                  ),
                ],
              ),
            ),
            drawerTile
          ])
        : drawerTile;
  }
}
