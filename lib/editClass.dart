import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/classes.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class EditClass extends StatefulWidget {
  final int id;
  final Map classData;

  EditClass({this.id, this.classData});

  @override
  _EditClassState createState() => _EditClassState();
}

class _EditClassState extends State<EditClass> {
  int _loadCounter = 0;
  TextEditingController _classNameController = TextEditingController();
  ScrollController _scrollController = ScrollController();

  Map topics;
  List students;

  Set<int> _selectedTopics = Set();
  Set<int> _selectedStudents = Set();

  bool _savingClass = false;

  @override
  void initState() {
    jsonDecode(widget.classData['topics']).forEach((topic) {
      if (topic != null && topic != '')
        _selectedTopics.add((topic is String) ? int.parse(topic) : topic);
    });
    jsonDecode(widget.classData['students']).forEach((student) {
      if (student != null && student != '')
        _selectedStudents
            .add((student is String) ? int.parse(student) : student);
    });

    _classNameController.text = widget.classData['name'];

    globals.api.call('listStudents', context: context).then((data) {
      setState(() {
        students = data['response'];
        _loadCounter++;
      });
    });
    globals.api.call('listTopics', context: context).then((data) {
      List rawTopics = data['response'];
      Map subjects = Map();
      rawTopics.forEach((currentTopic) {
        if (!subjects.keys.contains(currentTopic['subject']))
          subjects[currentTopic['subject']] = [];
        subjects[currentTopic['subject']].add(currentTopic);
      });
      setState(() {
        _loadCounter++;
        topics = subjects;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          path: HelpPagePath.editClass, label: S.of(context).helpEditingClass),
      title: S.of(context).editClass,
      appBarLeading: IconButton(
        icon: FaIcon(FontAwesomeIcons.times),
        onPressed: () => Navigator.of(context).pop(),
        tooltip: S.of(context).close,
      ),
      appBarActions: <Widget>[
        Builder(
          builder: (context) => IconButton(
            icon: FaIcon(FontAwesomeIcons.trash),
            onPressed: () => deleteClass(context),
            tooltip: S.of(context).deleteThisClass,
          ),
        )
      ],
      body: (_loadCounter >= 2)
          ? TestAppScrollBar(
              controller: _scrollController,
              child: ListView(
                controller: _scrollController,
                children: <Widget>[
                  TestAppCard(children: [
                    Text(S.of(context).renameClass,
                        style: Theme.of(context).textTheme.headline6),
                    RichText(
                        text: TextSpan(
                      style: Theme.of(context).textTheme.bodyText2,
                      children: [
                        TextSpan(
                          text: S.of(context).hint,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                            text: S
                                .of(context)
                                .thinkAboutTheNameSometimesItsMoreSensfulToGive),
                        TextSpan(
                            text: '10b',
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(
                            text: S
                                .of(context)
                                .butAnywaySometimesYouNeedNamesLike),
                        TextSpan(
                            text: S.of(context).exampleClassName,
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(
                            text: S
                                .of(context)
                                .youShouldTalkAboutThisWithYourColleagues)
                      ],
                    )),
                    TextField(
                      controller: _classNameController,
                      decoration:
                          InputDecoration(labelText: S.of(context).className),
                    )
                  ]),
                  TestAppCard(children: <Widget>[
                    Text(S.of(context).topics,
                        style: Theme.of(context).textTheme.headline6),
                    Text("${_selectedTopics.length} " +
                        S.of(context).topicsSelected),
                    ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (c, index) {
                        String subjectName = topics.keys.toList()[index];
                        List<Widget> children = [];
                        topics[subjectName].forEach((topic) {
                          children.add(
                            CheckboxListTile(
                              value: (_selectedTopics
                                  .contains(int.parse(topic['id']))),
                              onChanged: (selected) {
                                setState(() {
                                  (selected)
                                      ? _selectedTopics
                                          .add(int.parse(topic['id']))
                                      : _selectedTopics
                                          .remove(int.parse(topic['id']));
                                });
                              },
                              title: Text(topic['name']),
                            ),
                          );
                        });
                        return (AwesomeExpansionTile(
                          title: Text(subjectName),
                          children: children,
                        ));
                      },
                      itemCount: topics.keys.length,
                    )
                  ]),
                  TestAppCard(children: <Widget>[
                    Text(S.of(context).students,
                        style: Theme.of(context).textTheme.headline6),
                    Wrap(
                      spacing: 8.0,
                      runSpacing: 4.0,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: _selectedStudents.map<Widget>((s) {
                        Map student = students.firstWhere((element) {
                          int id = (element['id'] is String)
                              ? int.parse(element['id'])
                              : element['id'];
                          return id == s;
                        },
                            orElse: () => setState(() {
                                  _selectedStudents.remove(s);
                                }));
                        return Chip(label: Text(student['name']));
                      }).toList()
                        ..insert(0, Text(S.of(context).lastInClass)),
                    ),
                    Text("${_selectedStudents.length} " +
                        S.of(context).studentsSelected),
                    StudentList(
                        studentData: students,
                        itemBuilder: (currentStudents) => CheckboxListTile(
                            title: Text(currentStudents['name']),
                            value: (_selectedStudents
                                .contains(int.parse(currentStudents['id']))),
                            onChanged: (selected) {
                              setState(() {
                                (selected)
                                    ? _selectedStudents
                                        .add(int.parse(currentStudents['id']))
                                    : _selectedStudents.remove(
                                        int.parse(currentStudents['id']));
                              });
                            }))
                  ])
                ],
              ),
            )
          : CenterProgress(),
      floatingActionButton: (!_savingClass)
          ? FloatingActionButton.extended(
              onPressed: saveClass,
              label: Text(S.of(context).saveClass),
              icon: FaIcon(FontAwesomeIcons.check),
            )
          : FloatingActionButton(
              onPressed: () {},
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            ),
    ));
  }

  void saveClass() {
    setState(() {
      _savingClass = true;
    });
    globals.api
        .call('updateClass',
            options: {
              'class': widget.id,
              'name': _classNameController.text,
              'students': _selectedStudents.toList(),
              'topics': _selectedTopics.toList()
            },
            context: context)
        .then((data) {
      Navigator.push(context, MaterialPageRoute(builder: (c) => Classes()));
    });
  }

  void deleteClass(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content:
                  Text(S.of(context).areYouSureToPermanentlyDeleteThisClass),
              actions: <Widget>[
                MaterialButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(S.of(context).cancel),
                ),
                MaterialButton(
                  onPressed: () {
                    setState(() {
                      _savingClass = true;
                    });
                    globals.api
                        .call('deleteClass',
                            options: {'class': widget.id}, context: context)
                        .then((data) => Navigator.of(context).push(
                            MaterialPageRoute(builder: (c) => Classes())));
                  },
                  child: Text(S.of(context).delete),
                )
              ],
            ));
  }
}

class StudentList extends StatefulWidget {
  final List studentData;
  final Widget Function(Map student) itemBuilder;

  StudentList({@required this.studentData, @required this.itemBuilder});

  @override
  _StudentListState createState() => _StudentListState();
}

class _StudentListState extends State<StudentList> {
  List alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

  @override
  Widget build(BuildContext context) {
    return (ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (c, index) {
        List studentsWithLetter;
        if (index == alphabet.length) {
          studentsWithLetter = widget.studentData.where((student) {
            return !alphabet.contains(
                (student['name'] is String && student['name'] != '')
                    ? student['name'].substring(0, 1).toUpperCase()
                    : '');
          }).toList();
        } else {
          studentsWithLetter = widget.studentData.where((student) {
            return ((student['name'] is String && student['name'] != '')
                    ? student['name'].substring(0, 1).toUpperCase()
                    : '') ==
                alphabet[index];
          }).toList();
        }
        List<Widget> children = [];
        studentsWithLetter.forEach((student) {
          children.add(widget.itemBuilder(student));
        });
        if (children.isEmpty)
          children.add(ListTile(
            leading: FaIcon(FontAwesomeIcons.infoCircle),
            title: Text(S.of(context).noStudentsAvailableForThisLetter),
          ));
        return (AwesomeExpansionTile(
          title: Text((index != alphabet.length)
              ? alphabet[index]
              : S.of(context).others),
          children: children,
        ));
      },
      itemCount: alphabet.length + 1,
    ));
  }
}
