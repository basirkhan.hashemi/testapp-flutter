import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app_flutter/generated/l10n.dart';
import 'package:test_app_flutter/login.dart';
import 'package:test_app_flutter/style.dart';
import 'package:url_launcher/url_launcher.dart';

class TestAppI {
  String baseUrl = 'https://testapp.ga/api/';
  String _sid = ''; // Session ID
  Future _doneFuture;

  TestAppI(BuildContext context) {
    _doneFuture = _init(context);
  }

  Future get initializationDone => _doneFuture;

  Future<Map<String, dynamic>> call(String command,
      {Map<String, dynamic> options, @required BuildContext context}) async {
    if (options == null) options = Map();

    options["job"] = command;

    Map<String, String> headers = {};
    if (_sid != '' && _sid != null) {
      options["sid"] = _sid;
    }

    String body = json.encode(options);

    //print(body);

    return await http
        .post(baseUrl, headers: headers, body: body)
        .then((response) async {
      if (response.statusCode == 200) {
        var res = json.decode(response.body);
        if (res["errors"].contains('NO_SESSION')) {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => LoginPage()));
        }
        //print(res);
        return (res);
      } else {
        return await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) => NetworkErrorDialog(
                  message: response.body,
                  callback: () =>
                      call(command, options: options, context: context),
                )).then((value) async => await value);
      }
    }).catchError((e) async {
      return await showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => NetworkErrorDialog(
                message: e.toString(),
                callback: () =>
                    call(command, options: options, context: context),
              )).then((value) async => await value);
    });
  }

  Future<bool> uploadImage(int exercise, http.MultipartFile file) async {
    var request = new http.MultipartRequest("POST",
        Uri.parse(baseUrl + '/?uploadImage&exercise=$exercise&sid=$_sid'));
    request.files.add(file);
    http.StreamedResponse response = await request.send();

    return (response.statusCode == 200);
  }

  Future<bool> _init(BuildContext context) async {
    try {
      _sid = await Preferences().fetch("sid");
    } catch (e) {
      _saveSID(context);
    }
    if (_sid == null || _sid == '') return (await _saveSID(context));
    return (true);
  }

  Future<bool> _saveSID(BuildContext context) async {
    return (call('loginState', context: context).then((body) {
      this._sid = body['sid'].toString();
      Preferences().save("sid", _sid);
      return (true);
    }));
  }

  void setBaseUrl(String newBase) {
    newBase = newBase.trim().toLowerCase();
    if (newBase.substring(0, 7) == 'http://') newBase = newBase.substring(7);
    if (newBase.substring(0, 8) != 'https://') {
      newBase.replaceFirst('^\\w+:', '');
      newBase = 'https://' + newBase;
    }
    if (newBase.substring(newBase.length - 1) != '/') newBase = newBase + '/';
    if (newBase.substring(newBase.length - 4) != 'api/')
      newBase = newBase + 'api/';
    baseUrl = newBase;
  }
}

class NetworkErrorDialog extends StatefulWidget {
  @required
  final String message;
  @required
  final Future<Map<String, dynamic>> Function() callback;

  const NetworkErrorDialog({Key key, this.message, this.callback})
      : super(key: key);

  @override
  _NetworkErrorDialogState createState() => _NetworkErrorDialogState();
}

class _NetworkErrorDialogState extends State<NetworkErrorDialog> {
  bool _dismissedByAction = false;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        print(_dismissedByAction);
        return _dismissedByAction;
      },
      child: AlertDialog(
        title: Text(S.of(context).errorDuringCommunicationWithServer),
        content: SingleChildScrollView(
          child: Column(
            children: [
              Text(S
                  .of(context)
                  .weUnfortunatelyCouldNotCommunicateWithOurServer),
              Image.asset(
                'assets/messages/network-error.png',
                scale: 2,
              ),
              AwesomeExpansionTile(
                title: Text(S.of(context).details),
                children: [
                  Text(S.of(context).weKnowTheFollowing + widget.message),
                ],
              ),
            ],
          ),
        ),
        actions: [
          TextButton(
              onPressed: () {
                setState(() {
                  _dismissedByAction = true;
                });
                launch(Uri.encodeFull(
                    'mailto:support@testapp.schule?subject=TestApp Error&body=${widget.message}'));
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).contactUs)),
          TextButton(
              onPressed: () async {
                setState(() {
                  _dismissedByAction = true;
                });
                Navigator.of(context).pop(widget.callback().then((value) {
                  return value;
                }));
              },
              child: Text(S.of(context).retry))
        ],
      ),
    );
  }
}

class Preferences {
  Future<bool> save(key, value) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();

      return prefs.setString(key, value);
    } catch (e) {
      return false;
    }
  }

  Future<String> fetch(key) async {
    try {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString(key);
    } catch (e) {
      return null;
    }
  }
}
