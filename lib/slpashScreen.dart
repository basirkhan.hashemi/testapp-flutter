import 'dart:ui';

import 'package:app_review/app_review.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:test_app_flutter/api.dart';
import 'package:test_app_flutter/dashboard.dart';
import 'package:test_app_flutter/login.dart';
import 'package:test_app_flutter/practise.dart';
import 'package:test_app_flutter/style.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isAdmin = false;
  bool _registeredNotifications = false;

  Future adminCheck() async {
    return globals.api.call('userInfo', context: context).then((data) {
      Preferences().save("Admin",
          (int.parse(data['response']['accesslevel']) >= 1).toString());
      globals.isAdmin = int.parse(data['response']['accesslevel']) >= 1;
    });
  }

  @override
  void initState() {
    // trying to load fitting locale
    if (['en', 'de', 'fr', 'tlh']
        .contains(window?.locale?.languageCode ?? 'en'))
      S.load(Locale(window?.locale?.languageCode ?? 'en'));
    // checking for locale override
    Preferences().fetch('language').then((languageCode) {
      switch (languageCode) {
        case 'en':
          S.load(Locale('en'));
          break;

        case 'de':
          S.load(Locale('de'));
          break;

        case 'fr':
          S.load(Locale('fr'));
          break;

        case 'tlh':
          S.load(Locale('tlh'));
          break;
        default:
          break;
      }
    });
    globals.api = TestAppI(context);
    checkLogin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!_registeredNotifications) _registerNotifications();
    return Scaffold(
        body: SplashBackground(
      child: Center(
        child: SingleChildScrollView(
            child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Hero(
                tag: "centralBear",
                child: Image.asset('assets/bears/bear-standard.png',
                    scale: (MediaQuery.of(context).size.width < 768) ? 1 : 1.5),
              ),
              CenterProgress(),
              Padding(
                padding: const EdgeInsets.only(top: 24),
                child: Text(
                  S.of(context).slogan,
                  style: Theme.of(context).textTheme.headline5,
                ),
              )
            ],
          ),
        )),
      ),
    ));
  }

  void _registerNotifications() async {
    try {
      if (!kIsWeb && Theme.of(context).platform == TargetPlatform.iOS) {
        AppReview.requestReview.then((onValue) {
          print(onValue);
        });
      }

      /*_registeredNotifications = true;
      if (kIsWeb ||
          (Theme.of(context).platform != TargetPlatform.iOS &&
              Theme.of(context).platform != TargetPlatform.android)) return;
      var pushDisabled = (await Preferences().fetch('pushDisabled')) == 'true';
      if (!pushDisabled) {
        await TestAppPush.requestPermission();
        TestAppPush.updateSchedule(context: context);
      }*/
    } catch (e) {
      print('Error registering notifications.');
    }
  }

  void checkLogin() {
    globals.api.initializationDone.then((newVal) async {
      globals.api.call('loginState', context: context).then((data) async {
        if (data["response"] == true) {
          try {
            // If information stored, only verifying again
            await Preferences().fetch("sid");
            adminCheck();
          } catch (e) {
            // If no admin info saved, **awaiting** fetching them
            await adminCheck();
          }
          bool launchedByPush = false;
          /*
          try {
            launchedByPush = (await FlutterLocalNotificationsPlugin()
                    .getNotificationAppLaunchDetails())
                .didNotificationLaunchApp;
          } catch (e) {
            print('Error checking notifications.');
          }*/
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    (launchedByPush) ? Practise() : Dashboard()),
          );
        } else {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => LoginPage()),
          );
        }
      }).catchError((error) {
        print('API Error: ' + error.toString());
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('No internet connection.'),
          action: SnackBarAction(label: 'Retry', onPressed: checkLogin),
        ));
      });
    });
  }
}

class SplashBackground extends StatefulWidget {
  final Widget child;

  const SplashBackground({Key key, this.child}) : super(key: key);

  @override
  _SplashBackgroundState createState() => _SplashBackgroundState();
}

class _SplashBackgroundState extends State<SplashBackground>
    with WidgetsBindingObserver {
  @override
  void didChangePlatformBrightness() {
    print(WidgetsBinding.instance.window
        .platformBrightness); // should print Brightness.light / Brightness.dark when you switch
    super.didChangePlatformBrightness(); // make sure you call this
  }

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
        builder: (context, orientation) => Container(
              constraints: BoxConstraints.expand(),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/splashscreen/Splash ' +
                          (orientation == Orientation.portrait
                              ? 'Portrait ' +
                                  (MediaQuery.of(context).devicePixelRatio *
                                              MediaQuery.of(context)
                                                  .size
                                                  .width >
                                          1080
                                      ? '5k'
                                      : '2k')
                              : 'Landscape ' +
                                  (MediaQuery.of(context).devicePixelRatio *
                                              MediaQuery.of(context)
                                                  .size
                                                  .width >
                                          1920
                                      ? '5k'
                                      : '2k')) +
                          (MediaQuery.of(context).platformBrightness ==
                                  Brightness.dark
                              ? ' dark.png'
                              : '.png')),
                      fit: BoxFit.cover)),
              child: widget.child,
            ));
  }
}
