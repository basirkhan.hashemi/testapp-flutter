/// Simple pie chart example.
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import 'generated/l10n.dart';

class ScoreChart extends StatelessWidget {
  final double score;
  final String labelText;
  final double height;

  ScoreChart(this.score, {this.labelText = '', this.height = 200});

  @override
  Widget build(BuildContext context) {
    String _labelText = labelText;
    if (_labelText == '') _labelText = S.of(context).your;
    var data = [
      new ScoreChartData(S.of(context).wrong, 100 - score, Colors.red),
      new ScoreChartData(S.of(context).correct, score, Colors.green),
    ];
    var series = [
      new charts.Series(
        id: 'Clicks',
        domainFn: (ScoreChartData scoreData, _) => scoreData.label,
        measureFn: (ScoreChartData scoreData, _) => scoreData.score,
        colorFn: (ScoreChartData scoreData, _) => scoreData.color,
        data: data,
      ),
    ];
    var chart = new charts.PieChart(
      series,
      animate: true,
    );
    var chartWidget = new Padding(
      padding: new EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          new SizedBox(
            height: height,
            child: chart,
          ),
          Text(_labelText +
              " " +
              S.of(context).score +
              ": " +
              score.toStringAsFixed(1) +
              '%')
        ],
      ),
    );
    return chartWidget;
  }
}

class ScoreChartData {
  final String label;
  final double score;
  final charts.Color color;

  ScoreChartData(this.label, this.score, Color color)
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}
